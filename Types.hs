module Types where

import Graphics.Rendering.OpenGL
import Linear as L
import Data.IORef
import qualified Graphics.UI.GLFW as G
import Data.Set (Set)
import qualified Data.Set as S

data Descriptor = Descriptor VertexArrayObject Program

type Point = (GLfloat, GLfloat, GLfloat)
type NormalsPoint = (GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat)

data State = State {
    descriptors :: [Descriptor],
    camera :: Camera,
    nVerts :: NumArrayIndices,
    lastFrameTime :: Float,
    keyRefs :: IORef (Set G.Key),
    mouseRef :: IORef MouseInfo
}

data MouseInfo = MouseInfo {
    prevPos :: Maybe (Double,Double),
    prevPitch :: Double,
    prevYaw :: Double,
    frontVec :: V3 GLfloat
} deriving Show


data Camera = Camera {
    cameraPos :: V3 GLfloat,
    cameraFront :: V3 GLfloat,
    cameraUp :: V3 GLfloat
} deriving Show