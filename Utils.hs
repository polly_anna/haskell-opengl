module Utils where    

import Types
import Linear
import Graphics.Rendering.OpenGL
import Control.Monad (unless, when)

if' :: Bool -> a -> a -> a
if' True  x _ = x
if' False _ y = y

unless' :: Monad m => m Bool -> m () -> m ()
unless' action falseAction = do
    b <- action
    unless b falseAction

maybe' :: Maybe a -> b -> (a -> b) -> b
maybe' m nothingRes f = case m of
    Nothing -> nothingRes
    Just x  -> f x

toArray :: V4 (V4 GLfloat) -> [GLfloat]
toArray row@(V4 c1 c2 c3 c4) = ((toArrayVect c1 ++ toArrayVect c2) ++ toArrayVect c3) ++ toArrayVect c4

toArrayVect :: V4 GLfloat -> [GLfloat]
toArrayVect vect@(V4 x y z w) = [x,y,z,w]

toRadians = realToFrac . (*(pi/180)) :: Double -> GLfloat

v3toVertex3 :: V3 GLfloat -> Vertex3 GLfloat
v3toVertex3 v@(V3 a b c) = Vertex3 a b c

pointToV3 :: Point -> V3 GLfloat
pointToV3 (a,b,c) = V3 a b c

appendPoints :: V3 GLfloat -> V3 GLfloat -> NormalsPoint
appendPoints fst@(V3 a b c) snd@(V3 a2 b2 c2) = (a,b,c,a2,b2,c2)

convertToFloarArr :: [NormalsPoint] -> [Float]
convertToFloarArr = concat . map (\(x,y,z,nx,ny,nz)-> [x, y, z, nx, ny, nz])

getVaoFromState :: State -> Int -> VertexArrayObject
getVaoFromState state index = do 
    let desc@(Descriptor vao program) = (descriptors state) !! index
    vao

getProgramFromState :: State -> Int -> Program
getProgramFromState state index = do 
    let desc@(Descriptor vao program) = (descriptors state) !! index
    program

transformMatrix :: V3 GLfloat -> M44 GLfloat
transformMatrix vector = mkTransformation (axisAngle (V3 (0::GLfloat) 0 0) 0) vector


-- convertVerts :: [(GLfloat, GLfloat, GLfloat)] -> [Float]
-- convertVerts = concat . map (\(x,y,z)-> [x, y, z])

-- convert :: ([(GLfloat, GLfloat, GLfloat)], [(GLfloat, GLfloat, GLfloat)]) -> ([Float],[Float])
-- convert (verts, norms) = (convertVerts verts, convertVerts norms)