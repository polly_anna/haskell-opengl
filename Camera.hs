module Camera where

import Isosphere
import Utils
import GUI
import Rendering
import Types
import LoadShaders

import Control.Monad (unless, when)
import Graphics.Rendering.OpenGL
import qualified Graphics.UI.GLFW as G
import System.Exit
import Control.Monad (forever)
import Data.IORef
import Linear as L
import Data.Set (Set)
import qualified Data.Set as S

lookAtMatrix :: Camera -> M44 GLfloat
lookAtMatrix (Camera pos front up) = L.lookAt pos (pos ^+^ front) up

setViewUniform ::  M44 GLfloat -> Program -> IO()
setViewUniform lookAtMat program = do
    viewMatrix <- newMatrix RowMajor $ toArray lookAtMat :: IO (GLmatrix GLfloat)

    currentProgram $= Just program
    viewLocation <- get $ uniformLocation program "view"
    uniform viewLocation $= (viewMatrix)

    -- currentProgram $= Just lightPr
    -- viewLocation <- get $ uniformLocation lightPr "view"
    -- uniform viewLocation $= (viewMatrix)

updateCamera :: Set G.Key -> GLfloat -> Camera -> Camera
updateCamera keySet speed cam@(Camera pos front up) = let
    moveVector = S.foldr (\key vec -> case key of
            G.Key'W -> vec ^+^ front
            G.Key'S -> vec ^-^ front
            G.Key'A -> vec ^-^ L.normalize (cross front up)
            G.Key'D -> vec ^+^ L.normalize (cross front up)
            _ -> vec
            ) (V3 0 0 0) keySet
    in cam {cameraPos = pos ^+^ (speed *^ L.normalize moveVector)}

idleCameraUpdate :: State -> Float -> IO Camera
idleCameraUpdate state currTime = do
    let deltaTime = currTime - (lastFrameTime state)
    let cameraSpeed = 5 * deltaTime
    keysDown <- readIORef (keyRefs state)
    let newCamera = updateCamera keysDown cameraSpeed (camera state)

    mouseInfo <- readIORef (mouseRef state)

    let newCamera' = newCamera{cameraFront = frontVec mouseInfo}
    return newCamera'