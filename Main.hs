{-# LANGUAGE FlexibleContexts #-}

module Main where

import Isosphere
import Utils
import GUI
import Rendering
import Camera
import Types
import LoadShaders

import Control.Monad (unless, when)
import Graphics.Rendering.OpenGL
import qualified Graphics.UI.GLFW as G
import System.Exit
import Control.Monad (forever)
import Data.IORef
import Linear as L
import Data.Set (Set)
import qualified Data.Set as S

_objectPos = V3 0 0 (-3.0)
_lightPos = V3 3 2.0 (-6.0)
_lastFrameTime = 0.0

lampVertices :: [GLfloat]
lampVertices = [ -0.5, -0.5, -0.5,
             0.5, -0.5, -0.5,
             0.5, 0.5, -0.5,
             0.5, 0.5, -0.5,
             -0.5, 0.5, -0.5,
             -0.5, -0.5, -0.5,

             -0.5, -0.5, 0.5,
             0.5, -0.5, 0.5,
             0.5, 0.5, 0.5,
             0.5, 0.5, 0.5,
             -0.5, 0.5, 0.5,
             -0.5, -0.5, 0.5,
             
             -0.5, 0.5, 0.5,
             -0.5, 0.5, -0.5,
             -0.5, -0.5, -0.5,
             -0.5, -0.5, -0.5,
             -0.5, -0.5, 0.5,
             -0.5, 0.5, 0.5,
             
             0.5, 0.5, 0.5,
             0.5, 0.5, -0.5,
             0.5, -0.5, -0.5,
             0.5, -0.5, -0.5,
             0.5, -0.5, 0.5,
             0.5, 0.5, 0.5,
             
             -0.5, -0.5, -0.5,
             0.5, -0.5, -0.5,
             0.5, -0.5, 0.5,
             0.5, -0.5, 0.5,
             -0.5, -0.5, 0.5,
             -0.5, -0.5, -0.5,
             
             -0.5, 0.5, -0.5,
             0.5, 0.5, -0.5,
             0.5, 0.5, 0.5,
             0.5, 0.5, 0.5,
             -0.5, 0.5, 0.5,
             -0.5, 0.5, -0.5]


main :: IO ()
main = do
    G.setErrorCallback (Just errorCallback)
    successfulInit <- G.init
    -- G.windowHint (G.WindowHint'OpenGLProfile G.OpenGLProfile'Core)
    if' successfulInit onSuccessInit exitFailure
        
      
onSuccessInit = do
    mw <- G.createWindow 640 480 "Icosphere example" Nothing Nothing
    maybe' mw (G.terminate >> exitFailure) $ \window -> do
        G.makeContextCurrent mw

        -- INPUTS
        initMouseRef <- newIORef $ MouseInfo Nothing 0 (-90) (V3 0 0 (-1))
        G.setCursorInputMode window G.CursorInputMode'Disabled
        G.setCursorPosCallback window (Just $ cursorPosCallback initMouseRef)

        initKeyRefs <- newIORef S.empty
        G.setKeyCallback window (Just $ keyCallback initKeyRefs)

        -- DESCRIPTORS
        let verts = addNormals (generateTrianglePoints $ getPoints 1)

        descriptor@(Descriptor vaoVerts program) <- initVertices $ convertToFloarArr verts
        descriptorLamp@(Descriptor vaoLamp programLamp) <- initLamp lampVertices

        -- CAMERA
        let initCamera = Camera {cameraPos = V3 0.0 0.0 0.0, cameraFront = V3 0.0 0.0 (-1.0), cameraUp = V3 0.0 1.0 0.0}

        -- STATE
        let state = State {
            descriptors = [descriptor, descriptorLamp],
            camera = initCamera,
            nVerts = fromIntegral $ length verts,
            lastFrameTime = _lastFrameTime,
            keyRefs = initKeyRefs,
            mouseRef = initMouseRef
        }

        G.setFramebufferSizeCallback window (Just $ setProjection state)

        mainLoop window state

        G.destroyWindow window
        G.terminate
        exitSuccess



mainLoop :: G.Window -> State -> IO ()
mainLoop w state = 
    unless' (G.windowShouldClose w) $ do
    (width, height) <- G.getFramebufferSize w
    
    setProjection state w width height
    setUpEnvironment

    let program = getProgramFromState state 0
    let programLamp = getProgramFromState state 1

    let triangles = getVaoFromState state 0
    let light = getVaoFromState state 1
    
    currTime <- maybe 0 realToFrac <$> G.getTime
    newCamera <- idleCameraUpdate state currTime

    -- Object
    currentProgram $= Just program
    let objModel = transformMatrix _objectPos
    modelMatrix <- newMatrix RowMajor $ toArray objModel :: IO (GLmatrix GLfloat)
    modelLocation <- get $ uniformLocation program "model"
    uniform modelLocation $= (modelMatrix)

    setViewUniform (lookAtMatrix newCamera) program

    lightPosLoc <- get $ uniformLocation program "lightPos"
    uniform lightPosLoc $= v3toVertex3 _lightPos

    lightPosLoc <- get $ uniformLocation program "viewPos"
    uniform lightPosLoc $= v3toVertex3 (cameraPos newCamera)

    renderObjects triangles (nVerts state) program

    -- Light
    currentProgram $= Just programLamp
    let lightModel = transformMatrix _lightPos
    modelMatrix <- newMatrix RowMajor $ toArray lightModel :: IO (GLmatrix GLfloat)
    modelLocation <- get $ uniformLocation programLamp "model"
    uniform modelLocation $= (modelMatrix)

    setViewUniform (lookAtMatrix newCamera) programLamp 

    renderObjects light (fromIntegral $ length lampVertices) programLamp

    let newState = state{camera = newCamera, lastFrameTime = currTime}

    G.swapBuffers w
    G.pollEvents
    mainLoop w newState