{-# LANGUAGE InstanceSigs #-}
module Rendering where

import Isosphere
import LoadShaders
import Utils
import Types

import Data.Int
import Control.Monad
import Foreign.Marshal.Array
import Foreign.Ptr
import Foreign.Storable
import Data.IORef
import Graphics.Rendering.OpenGL as GL
import qualified Graphics.UI.GLFW as G
import Linear as L

lightColor = Color3 1.0 1.0 (1.0 :: GLfloat)
objectColor = Color3 1.0 0.5 (0.31 :: GLfloat)

bufferOffset :: Integral a => a -> Ptr b
bufferOffset = plusPtr nullPtr . fromIntegral

initLamp :: [Float] -> IO Descriptor
initLamp lampVertices = do
    colors <- genObjectName
    bindVertexArrayObject $= Just colors
    
    let numLVertices = length lampVertices
    lampVertexBuffer <- genObjectName
    bindBuffer ArrayBuffer $= Just lampVertexBuffer
    
    withArray lampVertices $ \ptr -> do
        let size = fromIntegral (numLVertices * sizeOf (head lampVertices))
        bufferData ArrayBuffer $= (size, ptr, StaticDraw)
    
    let colorFirstIndex = 0
        colorPosition = AttribLocation 0
    vertexAttribPointer colorPosition $=
        (ToFloat, VertexArrayDescriptor 3 Float 0 (bufferOffset colorFirstIndex))
    
    vertexAttribArray colorPosition $= Enabled

    lightProgram <- loadShaders [
        ShaderInfo VertexShader (FileSource "shaders/lightVert.vert"),
        ShaderInfo FragmentShader (FileSource "shaders/lightFrac.frac")]

    return $ Descriptor colors lightProgram


initVertices:: [Float] -> IO Descriptor
initVertices vertices = do
    print vertices
    -- VERTICES
    triangles <- genObjectName
    bindVertexArrayObject $= Just triangles

    let numVertices = length vertices
    vertexBuffer <- genObjectName
    bindBuffer ArrayBuffer $= Just vertexBuffer

    withArray vertices $ \ptr -> do
        let size = fromIntegral (numVertices * sizeOf (head vertices))
        bufferData ArrayBuffer $= (size, ptr, StaticDraw)
    
    let sizeF = fromIntegral (6 * sizeOf (head vertices))

    let firstIndex = 0
        vPosition = AttribLocation 0
    vertexAttribPointer vPosition $=
        (ToFloat, VertexArrayDescriptor 3 Float sizeF (bufferOffset firstIndex))
    vertexAttribArray vPosition $= Enabled

    -- NORMALS
    -- let numNormals = length normals
    -- normalsBuffer <- genObjectName
    -- bindBuffer ArrayBuffer $= Just normalsBuffer

    -- withArray normals $ \ptr -> do
    --     let size = fromIntegral (numNormals * sizeOf (head normals))
    --     bufferData ArrayBuffer $= (size, ptr, StaticDraw)

    let nFirstIndex = 3 * sizeOf (head vertices)
        nPosition = AttribLocation 1
    vertexAttribPointer nPosition $=
        (ToFloat, VertexArrayDescriptor 3 Float sizeF (bufferOffset nFirstIndex))
    vertexAttribArray nPosition $= Enabled

    -- SHADER
    program <- loadShaders [
        ShaderInfo VertexShader (FileSource "shaders/triangles.vert"),
        ShaderInfo FragmentShader (FileSource "shaders/triangles.frac")]
    currentProgram $= Just program

    setUpColors program

    return $ Descriptor triangles program

setUpEnvironment = do
    clear [ColorBuffer, DepthBuffer]
    depthFunc $= Just Less
    clearColor $= Color4 0.1 0.1 0.1 1

setUpColors :: Program -> IO()
setUpColors program = do
    lightLoc <- get $ uniformLocation program "lightColor"
    uniform lightLoc $= lightColor

    objColorLoc <- get $ uniformLocation program "objectColor"
    uniform objColorLoc $= objectColor


renderObjects :: VertexArrayObject -> NumArrayIndices -> Program -> IO()
renderObjects vao nVerts program =  do
    currentProgram $= Just program

    bindVertexArrayObject $= Just vao
    drawArrays Triangles 0 nVerts


setProjection :: State -> G.FramebufferSizeCallback
setProjection state window width height = do
    let program = getProgramFromState state 0
    let programLamp = getProgramFromState state 1

    let ratio = fromIntegral width / fromIntegral height
    let projMat = L.perspective (toRadians 45) ratio 0.1 100.0

    projectionMatrix <- newMatrix RowMajor $ toArray projMat :: IO (GLmatrix GLfloat)

    projLocation <- get $ uniformLocation program "projection"
    uniform projLocation $= (projectionMatrix)

    projLocationLight <- get $ uniformLocation programLamp "projection"
    uniform projLocationLight $= (projectionMatrix)