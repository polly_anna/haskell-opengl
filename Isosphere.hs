module Isosphere where

import Graphics.Rendering.OpenGL
import Utils
import Linear as L
import Types 

-- CONSTANTS
_PI :: GLfloat
_PI = 3.1415926
    
_H_ANGLE :: GLfloat
_H_ANGLE = _PI / 180 * 72
    
_V_ANGLE :: GLfloat
_V_ANGLE = atan(1.0 / 2)
    
hAngle1 :: GLfloat
hAngle1 = (-_PI) / 2 - _H_ANGLE / 2
    
hAngle2 :: GLfloat
hAngle2 = (-_PI) / 2


addNormals :: [Point] -> [NormalsPoint]
addNormals [] = []
addNormals verts@(a:b:c:ps) = do
    let res = addNormals ps
    let aVec = pointToV3 a
    let bVec = pointToV3 b
    let cVec = pointToV3 c
    let diff1 = bVec ^-^ aVec
    let diff2 = cVec ^-^ bVec
    let normal = L.cross diff1 diff2 
    let normal' = normal ^/ (L.quadrance normal)
    (([appendPoints aVec normal'] ++ [appendPoints bVec normal']) ++ [appendPoints cVec normal']) ++ res

subdivide :: [Point] -> Int -> GLfloat -> [Point]
subdivide triangles nSubs r
    | nSubs < 1 = triangles
    | otherwise = do
        let arr = subdivide triangles (nSubs - 1) r
        subDivideTriangles arr ((length arr) - 1) r

subDivideTriangles :: [Point] -> Int -> GLfloat -> [Point]
subDivideTriangles triangles n r
    | n < 0 = []
    | otherwise = do
        let res = subDivideTriangles triangles (n - 3) r
        let v3 = triangles !! (n - 2)
        let v2 = triangles !! (n - 1)
        let v1 = triangles !! n
        let newV1 = computeNewVertice v1 v2 r
        let newV2 = computeNewVertice v2 v3 r
        let newV3 = computeNewVertice v1 v3 r
        res ++ [v1, newV1, newV3, newV1, v2, newV2, newV1, newV2, newV3, newV3, newV2, v3]

computeNewVertice (x1,y1,z1) (x2,y2,z2) r = do
    let (x, y, z) = (x1+x2, y1+y2, z1+z2)
    let scale =  r / sqrt (x * x + y * y + z * z)
    (x*scale, y*scale, z*scale)


generateTrianglePoints ps = do
        let start = ps !! 0
        let end = ps !! 11
        getTriangles ps start end 1
    
getTriangles ps start end index
        | index > 5 = []
        | otherwise = do
            let arr = getTriangles ps start end (index+1)
            let f1 = ps !! index
            let f2 = if' (index == 5) (ps!!1) (ps !! (index + 1))
            let s1 = ps !! (index + 5)
            let s2 = if' (index == 5) (ps!!6) (ps !! (index + 6))
            ((([start, f1,  f2] ++ [f1, s1, f2]) ++ [f2, s1, s2]) ++ [s1, end, s2]) ++ arr
    

getPoints :: GLfloat -> [Point]
getPoints r = do
        let start = [(0.0, 0.0, r)]
        let fst = fillFirstRow 1 r hAngle1
        let snd = fillSecondRow 1 r hAngle2
        let last = [(0.0, 0.0, -r)]
        ((start ++ fst) ++ snd) ++ last
    
fillFirstRow :: Int -> GLfloat -> GLfloat -> [Point]
fillFirstRow index r hAng1
        | index > 5 = []
        | otherwise = do
            let arr = fillFirstRow (index+1) r (hAng1 + _H_ANGLE)
    
            let z = r * (sin _V_ANGLE)
            let xy = r * (cos _V_ANGLE)
    
            let p = ((xy * (cos hAng1)), (xy * (sin hAng1)), z)
            (p : arr)
    
fillSecondRow :: Int -> GLfloat -> GLfloat -> [Point]
fillSecondRow index r hAng2
        | index > 5 = []
        | otherwise = do
            let arr = fillSecondRow (index+1) r (hAng2 + _H_ANGLE)
            
            let z = r * (sin _V_ANGLE)
            let xy = r * (cos _V_ANGLE)
            
            let p = ((xy * (cos hAng2)), (xy * (sin hAng2)), -z)
            p : arr

