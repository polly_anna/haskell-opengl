module GUI where

import Utils
import Types

import System.Exit
import System.IO
import Data.IORef
import Data.Fixed
import Graphics.Rendering.OpenGL as GL
import qualified Graphics.UI.GLFW as G
import Data.Set (Set)
import qualified Data.Set as S
import Control.Monad
import Linear as L


errorCallback :: G.ErrorCallback
errorCallback err description = hPutStrLn stderr description

cursorPosCallback :: IORef MouseInfo -> G.CursorPosCallback
cursorPosCallback ref window xpos ypos = do
    modifyIORef ref $ \oldInfo -> let
        (lastX, lastY) = case prevPos oldInfo of
            Nothing -> (xpos,ypos)
            (Just (lastX,lastY)) -> (lastX,lastY)
        sensitivity = 0.02
        xoffset = (xpos - lastX) * sensitivity
        yoffset = (lastY - ypos) * sensitivity
        lastX' = xpos
        lastY' = ypos
        oldPitch = prevPitch oldInfo
        oldYaw = prevYaw oldInfo
        newYaw = (oldYaw + xoffset) `mod'` 360.0
        newPitch = min (max (oldPitch + yoffset) (-89)) 89
        pitchR = toRadians newPitch
        yawR = toRadians newYaw
        front = L.normalize $ V3 (cos yawR * cos pitchR) (sin pitchR) (sin yawR * cos pitchR)
        in MouseInfo (Just (lastX',lastY')) newPitch newYaw front

keyCallback :: IORef (Set G.Key) -> G.KeyCallback
keyCallback ref window key scanCode keyState modKeys = do
    case keyState of
        G.KeyState'Pressed -> do
            modifyIORef ref (S.insert key)
        G.KeyState'Released -> modifyIORef ref (S.delete key)
        _ -> return ()
    when (key == G.Key'Escape && keyState == G.KeyState'Pressed)
        (G.setWindowShouldClose window True)
